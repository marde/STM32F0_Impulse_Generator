/*
******************************************************************************
Project:	STM32F050 Impulse generator
File:     main.c

Encoder is connected to PA0 and PA1 pins
Switch on encoder is connected to PA2 pin

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

******************************************************************************
*/

/* Includes */
#include "stm32f0xx.h"
#include "stm32f0xx_conf.h"
#include "lcd.h"
#include <string.h>
#include <stdlib.h>

/* global variables */
  uint32_t g_start = 0;
  uint32_t g_start_time = 0;
  uint32_t g_stop = 0;
  uint32_t g_stop_time = 0;
  uint32_t g_time = 0;

#define SWITCH_PIN	GPIO_Pin_2


/* Private macro */
/* Private variables */
/* Private function prototypes */
/* Private functions */



/**
  * @brief  Configure a SysTick Base time to 10 ms.
  * @param  None
  * @retval None
  */
static void SysTickConfig(void)
{
	/* fastest clock surce */
  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);

  /* Setup SysTick Timer for 10us interrupts  */
  if (SysTick_Config(SystemCoreClock / 100000))
  {
    /* Capture error */
    while (1);
  }
  /* Configure the SysTick handler priority */
  NVIC_SetPriority(SysTick_IRQn, 0x0);
}


/**
  * @brief  Configure the TIM pins and the TIM IRQ Handler.
  * @param  None
  * @retval None
  */
static void TIM_Config(void)
{
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  //NVIC_InitTypeDef NVIC_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;

  /* TIM2 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

  /* GPIOA clock enable */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

  /* TIM2 channel 2 pin configuration */
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0 | GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Connect TIM pins to AF2 */
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_2);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_2);

  /* -----------------------------------------------------------------------
    TIM2 Configuration: Encoder mode1:

    In this example TIM2 input clock (TIM2CLK) is set to APB1 clock (PCLK1).
      TIM3CLK = PCLK1
      PCLK1 = HCLK
      => TIM3CLK = HCLK = SystemCoreClock

    To get TIM3 counter clock at 6 MHz, the prescaler is computed as follows:
       Prescaler = (TIM3CLK / TIM3 counter clock) - 1
       Prescaler = ((SystemCoreClock) /6 MHz) - 1

   TIM3 is configured to interface with an encoder:
   - The encoder mode is encoder mode1: Counter counts up/down on TI2 rising edge
   depending on TI1 level

   - The Autoreload value is set to 399, so the encoder round is 400 TIM counter clock.


    Note:
     SystemCoreClock variable holds HCLK frequency and is defined in system_stm32f0xx.c file.
     Each time the core clock (HCLK) changes, user had to call SystemCoreClockUpdate()
     function to update SystemCoreClock variable value. Otherwise, any configuration
     based on this variable will be incorrect.
  ----------------------------------------------------------------------- */


  /* Time base configuration */
  TIM_TimeBaseStructure.TIM_Period = 0xFFFFFFFF;
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

  /* Prescaler configuration */
  //TIM_PrescalerConfig(TIM3, PrescalerValue, TIM_PSCReloadMode_Immediate);

  TIM_EncoderInterfaceConfig(TIM2, TIM_EncoderMode_TI12, TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);

  /* TIM Interrupts enable */
  //TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

  /* set starting position to middle */
  TIM_SetCounter (TIM2, 0x7FFFFFFF );

  /* TIM2 enable counter */
  TIM_Cmd(TIM2, ENABLE);

  /* Enable the TIM3 gloabal Interrupt */
  /*
  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
  */
}

/**
  * @brief  Time of Event
  * @param  Time of start of event in systicks
  * @retval Time of event in systicks
  */
uint32_t DeltaTime(uint32_t cas)
{
	uint32_t nyni, delta;
	nyni = g_time;
	if (cas<=nyni)
		delta=nyni-cas;
	else
		delta=0xFFFFFFFF - cas + nyni;
	return delta;
}



/**
**===========================================================================
**
**  Abstract: main program
**
**===========================================================================
*/
int main(void)
{
  int32_t i = 0;
  char text[] = "Pulse Generator ";
  uint32_t encoder_pos = 0x7FFFFFFF;	//middle
  /* change=0 : changing Time Off / Time On;  change=1 : changing the selected time */
  uint8_t change = 0;
  /* selected=0 : changing Time Off ; selected=1 : changing Time On */
  uint8_t selected = 0;
  uint8_t flag_lcd = 0;		// flag if is necessary something print
  uint32_t time_stamp;

  SysTickConfig();
  GPIO_LCD_Inicializace();

  /* welcome text */
  printLCD(text);
  strcpy(text, "ver. 1.0");
  LCD_Cursor2line();
  printLCD(text);
  delay_ms(500);

  TIM_Config();

  g_start = 0xFFF;
  g_stop = 0xFFF;

  time_stamp = g_time;

  /* Infinite loop */
  while (1)
  {
		flag_lcd = 0;
  	if (DeltaTime(time_stamp > 10))
  	{
  		/* uplynulo 10 ms */
  		i = (int32_t) (encoder_pos - TIM2->CNT);
  		encoder_pos = TIM2->CNT;
  		/* if the key is pressed */
  		if (GPIO_ReadInputDataBit(GPIOA, SWITCH_PIN) == 0)
  		{
				flag_lcd = 1;
  			if (change == 0)
  			{
  				/* now changing the value of variable */
  				change = 1;
  			}
  			else
  			{
  				/* now changing what will be changed */
  				change = 0;
  			}
  		} else
  		{
  			/* no button is pressed, so we have to check encoder change */
  			if (change == 0)
  			{
  				if (abs(i)>0) flag_lcd = 1;

  				/* now changing the value of variable - exponential growth */
  				if (abs(i) < 50)
  				{
  					i *= 50;
  				} else if (abs(i) < 30)
  				{
  					i *= 25;
  				} else if (abs(i) < 15)
  				{
  					i *= 10;
  				} else if (abs(i) < 5)
  				{
  					i *= 5;
  				}
  				if (selected == 0)
  				{
  					/* changing Time Off */
  					if ((i<0) && (abs(i)>=g_start)) g_start = 1;
  					else g_start += i;
  					if (g_start > 99999) g_start = 99999;
  				} else
  				{
  					/* changing Time On */
  					if ((i<0) && (abs(i)>=g_stop)) g_stop = 0;
  					else g_stop += i;
  					if (g_stop > 99999) g_stop = 99999;
  					/* stop can be zero for short Dirac pulse */
  				}
  			}	// change == 0
  			else
  			{	// change != 0
  				/* now changing what will be changed */
  				if (selected == 0)
  				{
  					/* changing Time Off */
  					if (i>0) selected = 1;
  				} else
  				{
  					/* changing Time On */
  					if (i<0) selected = 0;
  				}
  			}
  		}
  		/* now we have print to LCD */
  		if(flag_lcd == 1)
  			LCD_Disp(change, selected, g_start, g_stop);
  	} //if (DeltaTime(time_stamp > 10))
  } //  while (1)
}



