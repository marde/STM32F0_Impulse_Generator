/* STM32 VL Discovery Library
 * LCD driver  LCD.h
 * ver 1.0
 *  Created on: Oct 17, 2010
 *      Author: mard@ mcu.cz
 *  http: mcu.cz
 */

#ifndef LCD_H_
#define LCD_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_conf.h"


/** @defgroup Exported_Types		*/

/** @defgroup Exported_Functions	*/

void delay_us(uint16_t cas);
void delay_ms(uint16_t cas);
void GPIO_LCD_Inicializace(void);
void LCD_Init(void);
void PosliPrikazLCD(uint8_t cmd);
void PosliZnakLCD(uint8_t cmd);
void LCD_Clear(void);
void LCD_CursorHome(void);
void LCD_Cursor2line(void);
void LCD_CursorON(void);
void LCD_CursorOFF(void);
void LCD_TextScrollON(void);
void LCD_TextScrollOFF(void);
void printLCD(char* retezec);
void LCD_Disp(uint8_t state, uint8_t what, uint32_t offtime, uint32_t ontime);

/** @defgroup Defines	*/


/* z LCD necteme jen zapisujeme, tedy RW pin je trvale na LOW */
/* #define LCD_RW	GPIO_Pin_13	*/

#define LCD_E		GPIO_Pin_15
#define LCD_RS	GPIO_Pin_12
#define LCD_D4	GPIO_Pin_8
#define LCD_D5	GPIO_Pin_9
#define LCD_D6	GPIO_Pin_10
#define LCD_D7	GPIO_Pin_11
#define LCD_PORT	GPIOA


#endif /* LCD_H_ */
