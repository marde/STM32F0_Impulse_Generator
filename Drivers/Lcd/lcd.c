/* STM32 VL Discovery Library
 * LCD driver  LCD.c
 * ver 1.0
 *  Created on: Oct 17, 2010
 *      Author: mard@ mcu.cz
 *  http: mcu.cz
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx.h"
#include "LCD.h"
#include <string.h>


/** @defgroup Private_TypesDefinitions	*/
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructureMicro = {36,TIM_CounterMode_Up,0,0,0};
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructureMili =  {36000,TIM_CounterMode_Up,0,0,0};
/** @defgroup Private_Defines			*/

/** @defgroup Private_Macros			*/
#define WAIT __asm__("mov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\t"\
                     "mov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\t"\
                     "mov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\t"\
                     "mov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\t"\
                     "mov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\t"\
                     "mov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\t"\
                     "mov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\tmov r8,r8\n\t")

/** @defgroup Private_Variables			*/

/** @defgroup Private_FunctionPrototypes*/

/** @defgroup Private_Functions			*/

/**
  * @brief  wait n microsecond
  * @param  wait_time : time to wait in microsecond
  * @retval None
  */
void delay_us(uint16_t cas)
{
  //Supply APB1 Clock
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

  /* Time base configuration */
  TIM_TimeBaseStructureMicro.TIM_Period = ((cas+1) * 2)-1;
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructureMicro);

  TIM_SelectOnePulseMode(TIM3, TIM_OPMode_Single);

  TIM_SetCounter(TIM3,2);

  /* TIM enable counter */
  TIM_Cmd(TIM3, ENABLE);
  while (TIM_GetCounter(TIM3)){};
  /* TIM disable counter */
  TIM_Cmd(TIM3, DISABLE);
}

/**
  * @brief  wait n millisecond
  * @param  wait_time : time to wait in millisecond
  * @retval None
  */
void delay_ms(uint16_t cas)
{
  //Supply APB1 Clock
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

  /* Time base configuration */
  TIM_TimeBaseStructureMili.TIM_Period = ((cas+1) * 2)-1;
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructureMili);

  TIM_SelectOnePulseMode(TIM3, TIM_OPMode_Single);

  TIM_SetCounter(TIM3,2);

  /* TIM enable counter */
  TIM_Cmd(TIM3, ENABLE);
  while (TIM_GetCounter(TIM3)){};
  /* TIM disable counter */
  TIM_Cmd(TIM3, DISABLE);
}


/**
  * @brief  Configure the GPIO Pins for LCD.
  * @param  None
  * @retval : None
  */
void GPIO_LCD_Inicializace(void)
{

  GPIO_InitTypeDef GPIO_InitStructure;

#if defined (STM32F051)
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC, ENABLE);
  GPIO_InitStructure.GPIO_Pin = LCD_E | LCD_RS | LCD_D4 | LCD_D5
		  | LCD_D6 | LCD_D7;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(LCD_PORT, &GPIO_InitStructure);

#else
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);

  GPIO_InitStructure.GPIO_Pin = LCD_E | LCD_RS | LCD_D4 | LCD_D5
		  | LCD_D6 | LCD_D7;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(LCD_PORT, &GPIO_InitStructure);
#endif
}

/**
  * @brief  Send 4-bit Commend to TCD
  * @param  command
  * @retval : None
  */
void Posli4bitLCD(uint8_t cmd)
{
	if (cmd & 0b00010000)
	{
		GPIO_SetBits(LCD_PORT, LCD_D4);
	} else {
		GPIO_ResetBits(LCD_PORT, LCD_D4);
	}
	if (cmd & 0b00100000)
	{
		GPIO_SetBits(LCD_PORT, LCD_D5);
	} else {
		GPIO_ResetBits(LCD_PORT, LCD_D5);
	}
	if (cmd & 0b01000000)
	{
		GPIO_SetBits(LCD_PORT, LCD_D6);
	} else {
		GPIO_ResetBits(LCD_PORT, LCD_D6);
	}
	if (cmd & 0b10000000)
	{
		GPIO_SetBits(LCD_PORT, LCD_D7);
	} else {
		GPIO_ResetBits(LCD_PORT, LCD_D7);
	}
	GPIO_SetBits(LCD_PORT, LCD_E);						//E=1:Enable
}


/**
  * @brief  LCD Init
  * @param  None
  * @retval : None
  */
void LCD_Init(void)
{
  // nastavime 8-bitovy komunikacni mod !! (protoze nevime zda uz displej byl prepnut nebo ne)

  GPIO_ResetBits(LCD_PORT, LCD_RS);						//RS=0:Instruction
  GPIO_ResetBits(LCD_PORT, LCD_E);						//E=0:Disable
  delay_ms(15);
  Posli4bitLCD(0b00110000);
  delay_ms(10);
  GPIO_ResetBits(LCD_PORT, LCD_E);					//E=0:Disable
  Posli4bitLCD(0b00110000);
  delay_ms(10);
  GPIO_ResetBits(LCD_PORT, LCD_E);					//E=0:Disable

  // ted by mel byt display v 8-bitovem modu a definitivne ho prepneme do 4-bitoveho modu!
  Posli4bitLCD(0b00100000);
  delay_ms(10);
  GPIO_ResetBits(LCD_PORT, LCD_E);					//E=0:Disable
  // nyni jsme ve 4-bitovem rezimu
  // Prikaz 0 0 1 DL N F - -
  // N = pocet radek 1=2 radky, 0=1 radka
  // DL = bitu komunikace 1=8bitu, 0=4bity
  // F = Font 1=5x10bodu, 0=5x8 bodu
  PosliPrikazLCD(0b00101000);
  // zvedej sam adresu, posunuj kurzor
  PosliPrikazLCD(0b00001110);
}

/**
  * @brief  Send 8-bit Commend to TCD
  * @param  command
  * @retval : None
  */
void PosliPrikazLCD(uint8_t cmd)	//v promenne cmd mame zaslany typ prikazu
{
	Posli4bitLCD(cmd);
	delay_ms(1);
	GPIO_ResetBits(LCD_PORT, LCD_E);					//E=0:Disable
	cmd = cmd<<4;
	Posli4bitLCD(cmd);
	delay_ms(1);
	GPIO_ResetBits(LCD_PORT, LCD_E);					//E=0:Disable
}


/**
  * @brief  Send 4-bit Commend to TCD
  * @param  command
  * @retval : None
  */
void PosliZnakLCD(uint8_t cmd)	//v prom�nn� cmd m�me poslany znak na LCD
{
	GPIO_SetBits(LCD_PORT, LCD_RS);					//RS=1:Data
	Posli4bitLCD(cmd);
	delay_ms(1);
	GPIO_ResetBits(LCD_PORT, LCD_E);					//E=0:Disable
	GPIO_ResetBits(LCD_PORT, LCD_RS);				//RS=0:Instruction
	delay_ms(1);
	cmd = cmd<<4;
	GPIO_SetBits(LCD_PORT, LCD_RS);					//RS=1:Data
	Posli4bitLCD(cmd);
	delay_ms(1);
	GPIO_ResetBits(LCD_PORT, LCD_E);					//E=0:Disable
	GPIO_ResetBits(LCD_PORT, LCD_RS);				//RS=0:Instruction
}

/**
  * @brief  Send 4-bit Commend to TCD
  * @param  command
  * @retval : None
  */
void LCD_Clear(void)
{
    PosliPrikazLCD(0b00000001);
}

/**
  * @brief  Set LCD Cursor to Home
  * @param  None
  * @retval : None
  */
void LCD_CursorHome(void)
{
    PosliPrikazLCD(0b00000010);
}

/**
  * @brief  Set LCD Cursor to 2 line
  * @param  None
  * @retval : None
  */
void LCD_Cursor2line(void)
{
    PosliPrikazLCD(0b11000000);
}

/**
  * @brief  Set LCD Cursor ON
  * @param  None
  * @retval : None
  */
void LCD_CursorON(void)
{
    PosliPrikazLCD(0b00001110);
}

/**
  * @brief  Set LCD Cursor OFF
  * @param  None
  * @retval : None
  */
void LCD_CursorOFF(void)
{
    PosliPrikazLCD(0b00001100);
}

/**
  * @brief  Set Text scrolling ON
  * @param  None
  * @retval : None
  */
void LCD_TextScrollON(void)
{
    PosliPrikazLCD(0b00000111);
}

/**
  * @brief  Set Text scrolling OFF
  * @param  None
  * @retval : None
  */
void LCD_TextScrollOFF(void)
{
    PosliPrikazLCD(0b00000110);
}


/**
  * @brief  String print to LCD
  * @param  string pointer
  * @retval : None
  */
void printLCD(char* retezec)		//posila se odkaz na retezec
{
	uint8_t i=0;
	// pro sichr si zkontrolujeme pointer
	if (!retezec) return;
	while(retezec[i]!='\0')
	{
		PosliZnakLCD(retezec[i]);
		i++;
	}
}

/* reverse:  reverse string s in place */
void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}


/* itoa:  convert n to characters in s */
uint8_t itoa(int n, char s[])
 {
    int i, sign;

     if ((sign = n) < 0)  /* record sign */
         n = -n;          /* make n positive */
     i = 0;
     do {       /* generate digits in reverse order */
         s[i++] = n % 10 + '0';   /* get next digit */
     } while ((n /= 10) > 0);     /* delete it */
     if (sign < 0)
         s[i++] = '-';
     s[i] = '\0';
     reverse(s);
     return i;
 }



/**
  * @brief  Displaying on the LCD matrix
  * @param  state, selected, offtime, ontime
  * @retval : None
  */
void LCD_Disp(uint8_t state, uint8_t what, uint32_t offtime, uint32_t ontime)
{
	/*          123456789012
	 * Off [ms] 999.99 99.9k f
	 * On  [ms] 000.01 99.99 D [%]
	 * offtime, ontime = *10us
	 */

	int8_t i;
	uint8_t j;
	char radka[17];

	/* first line */
	for (i =0; i<16; i++) radka[i]=' ';
	j = itoa(offtime, radka);
	if (j<6) for (i = 5; i>=0; i--) radka[i]= radka[i+4-j];
	/* insert decimal point */
	radka[5] = radka[4];
	radka[4] = radka[3];
	radka[3] = '.';
	if (what == 0)
	{
		if (state == 0)
			radka[6] = '<';
		else
			radka[6] = '{';
	}
	radka[16] = '\0';
	/* frequency will be implemented later TODO */
	LCD_CursorHome();
	printLCD(radka);
	/* second line */
	for (i =0; i<16; i++) radka[i]=' ';
	j = itoa(ontime, radka);
	if (j<6) for (i = 5; i>=0; i--) radka[i]= radka[i+4-j];
	/* insert decimal point */
	radka[5] = radka[4];
	radka[4] = radka[3];
	radka[3] = '.';
	if (what == 0)
	{
		if (state == 0)
			radka[6] = '<';
		else
			radka[6] = '{';
	}
	radka[16] = '\0';
	/* duty will be implemented later TODO */
	LCD_Cursor2line();
	printLCD(radka);
}
